function XOut = FlattenData(X)
% Turns a cell array of m tweets to a m * d matrix of m tweets and a
% d-dimension one-hot encoding of whether a word exists in said tweet.
%
% Each coulumn of XOut represents 1 tweet, where each row of said column is
% 1 if the word exists in the tweet, and 0 if it does not. Duplicate words
% are only added once.

numTweets = length(X); % Number of tweets
if numTweets > 0 % Prevent error with empty data
    [d, ~] = size(X{1}); % get dimensionality of data
else
    write("Error, cell array has 0 tweets!");
    return;
end

% Flattened X initialisation
XOut = zeros(d,numTweets);

% For each tweet
for tweetNum = 1:numTweets
    
    tweet = X{tweetNum}; % Extract the mth tweet
    
    oneHot = max(tweet,[],2); % Add words without duplication
    
    XOut(:,tweetNum) = oneHot; % Save total one-hot in index for tweet
end

        
    
    